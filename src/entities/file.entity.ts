import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity('files')
export class File {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    src: string;
}
