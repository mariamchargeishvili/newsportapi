import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity('item_attachment_types')
export class ItemAttachmentType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
}
