import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {Item} from './item.entity';
import {ItemAttachmentType} from './item-attachment-type.entity';

@Entity('item_attachments')
export class ItemAttachment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(() => ItemAttachmentType)
    type: ItemAttachmentType;

    @ManyToOne(() => Item)
    item: Item;

    @ManyToOne(() => File)
    file: File;
}
