import {Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn} from 'typeorm';
import {ItemType} from './item-type.entity';
import {ItemAttachment} from './item-attachment.entity';
import {User} from './user.entity';

@Entity('items')
export class Item {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @ManyToOne(() => User)
    user: User;

    @ManyToOne(() => ItemType)
    type: ItemType;

    @OneToMany(() => ItemAttachment, attachment => attachment.item)
    attachments: ItemAttachment[];

    @ManyToMany(() => Item)
    @JoinTable()
    relations: Item[] = [];
}
