export interface BaseItemDto {
    id: number;
    title: string;
    description: string;
    type: ItemTypeDto;
    author: AuthorDto | null;
    createdAt: Date;
    updatedAt: Date;
    viewCount: number;
    shareCount: number;
}





export interface VideoDto extends BaseItemDto{
    content:DeltaDto,
    thumbnail:File,
}

export interface SportDto extends BaseItemDto {
    cover: FileDto;
    iframe: IFrameDto | null;
}

export interface TournamentDto extends BaseItemDto{
    cover: FileDto;
    iframe: IFrameDto | null;
}

export interface TeamDto  extends BaseItemDto{
    cover: FileDto;
    iframe: IFrameDto | null;
    logo: FileDto;
    stadium: StadiumDto | null;
}



export interface PostDto extends BaseItemDto {
    content: DeltaDto;
    image: File

}

export interface ArticleDto extends BaseItemDto {
    content: DeltaDto;
    image: File;
    outline: DeltaDto
}

export interface baseBlockItemDto{
    id:number;
    title: string;
}
export interface VideoBlockItemDto extends baseBlockItemDto{
    duration:string;
    video: File;
}
export interface SportBlockItemDto extends baseBlockItemDto{
    image: File;
}

export interface PostBlockItemDto extends baseBlockItemDto{
    image:File
}

export interface ArticleBlockItemDto extends baseBlockItemDto{
    image:File
}

export declare type ItemDto = SportDto | TournamentDto | TeamDto | PostDto | ArticleDto | VideoDto  ;

export interface AuthorDto {
    id: number;
    name: string;
    photo: FileDto;
}

export interface FileDto {
    id: number;
    src: string;
}

export interface ItemTypeDto {
    id: number;
    name: string;
}

export interface IFrameDto {
    src: string;
}


export interface ItemsBlockDto {
    id: number;
    title: string;
    filters: ItemBlockFilterDto[];
}

export interface ItemBlockFilterDto {
    name: string;
    query: {
        itemId: number,
        blockId: number,
        filterId: number,
        page: number,
    };
}

export interface AdsBlockDto {
    iframe: IFrameDto;
}

export declare type BlockDto = ItemsBlockDto | AdsBlockDto;

export interface StadiumDto {
    title: string;
    description: string;
    photo: FileDto;
}


export interface DeltaDto {

}


export interface Navigation {
    id: number;
    title: string;
    url: string;
    target: string;
    nodes?: Navigation[];
}

export interface Slide {
    id: number;
    title: string;
    titleColor: Color;
    description: DeltaDto;
    image: FileDto;
    url: string;
    target: string;
    iframe?: IFrameDto;
}


export interface Color {
    hex: string;
}

export interface BlockList {
    blocks: BlockDto[];
}

export interface List<T> {
    page: number;
    lastPage: number;
    items: T[];
}





